serverPort = process.env.PORT;
if(serverPort == null) {
    serverPort = 5000;
}
var harp = require("harp")
harp.server(__dirname, {port: serverPort}, function() { /* NOOP */})